package main

import (
	"net/http"
)

func main() {
	// Simple static webserver:
	(http.ListenAndServe(":3000", http.FileServer(http.Dir("./start/"))))
}
