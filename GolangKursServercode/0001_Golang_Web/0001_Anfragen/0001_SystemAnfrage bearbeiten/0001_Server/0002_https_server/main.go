package main

import (
	"net/http"
)

func main() {
	server := &http.Server{

		Addr: ":3000",
	}
	server.ListenAndServeTLS("/etc/letsencrypt/live/webprogrammieren.de/cert.pem", "/etc/letsencrypt/live/webprogrammieren.de/privkey.pem") //https Server startet und Port 3000 zuhÃ¶ren
}
