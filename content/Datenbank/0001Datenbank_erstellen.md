---
title: "Datenbank und Tabelle erstellen"
draft: false
weight: 1
---

## Datenbank erstellen

```sql
create database autobetrieb;  -- Datenbank erstellen

```  
#### Table erstellen

```sql

use autobetrieb; -- benutze die Datenbank "autobetrieb"

create table hersteller (
id int auto_increment primary key,
testeingabe text(100)
);
``` 