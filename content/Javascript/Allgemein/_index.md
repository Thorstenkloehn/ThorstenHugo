---
title: "Allgemein"
draft: false
weight: 1
---
## Javascript
### Video

* [PanjuTorials](https://www.youtube.com/watch?v=o40dGRLBl7A&list=PLiHzu4i2Hsb2uFxjGleWNBENdzp9SKm9V)
* [The Morpheus Tutorials](https://www.youtube.com/watch?v=UeZi8a99iS0&list=PLNmsVeXQZj7qOfMI2ZNk-LXUAiXKrwDIi)
* [listenwhatisayoh](https://www.youtube.com/watch?v=5WqO5u2at-A&list=PL7F80A6B471177E54)

### Freie Javacript Bücher

* [JavaScript und AJAX](http://openbook.rheinwerk-verlag.de/javascript_ajax/) openbook.rheinwerk-verlag.de

### Javascript Webseiten
