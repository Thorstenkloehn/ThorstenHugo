---
title: "System Anfragen bearbeiten"
draft: false
weight: 1
---

### Server Go
#### Server einstellen und starten
##### http Server
```go
package main

import (
	"net/http"
)

func main() {
server := &http.Server{  // Ein Server definiert Parameter zum Ausführen eines HTTP-Servers. 

    Addr: ":3000",   // Port Adresse
}

server.ListenAndServe() 
}



``` 
#### https Server

```go
package main

import (
	"net/http"
)

func main() {
server := &http.Server{    

    Addr: ":3000",   
}
server.ListenAndServeTLS("/etc/letsencrypt/live/webprogrammieren.de/cert.pem", "/etc/letsencrypt/live/webprogrammieren.de/privkey.pem") //https Server startet und Port 3000 zuhÃ¶ren
}
``` 
## FileServer
```go
package main

import (
	"net/http"
)

func main() {
	// Simple static webserver:
	(http.ListenAndServe(":3000", http.FileServer(http.Dir("./start/"))))
}


``` 

### http System Anfrage bearbeiten


#### handler
##### Basic Router
##### Gorilla  Mux
#### handlefunc
