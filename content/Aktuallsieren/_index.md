---
title: "Ubuntu aktualisieren"
draft: false
weight: 2
---
## Ubuntu aktualisieren

```bash  
ssh root@webprogrammieren.de ./start.sh
```

## Golang Aktualsieren

```bash
sudo rm -r /usr/local/go
sudo wget https://dl.google.com/go/go1.12.5.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.12.5.linux-amd64.tar.gz
reboot
```  

