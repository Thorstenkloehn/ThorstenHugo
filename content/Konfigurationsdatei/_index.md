---
title: "Konfigurationsdatei"
draft: false
weight: 12
---

* INI (Initialisierungsdatei)
* YAML (YAML Ain’t Markup Language)
* TOML (Tom's Obvious, Minimal Language)
* XML (Extensible Markup Language)
* JSON (JavaScript Object Notation)