---
title: "Static Generator"
draft: false
weight: 3
---
## Installieren und Download
* [hugo Installieren](https://github.com/gohugoio/hugo/releases)

## Hugo-Projekt erstellen
```bash
hugo new site ./
hugo new site Website Name
``` 
## Theme herunterladen und Installieren
* [Hugo Themen](https://themes.gohugo.io/)

## Hochladen




